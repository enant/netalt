// Dependencies
var express = require('express');
var path = require('path');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var fs = require('fs');
var https = require('https');

var database = require('./config/database.js');
var app = express();

// Middleware
app.use(logger('dev'))
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(cookieParser());
app.use(require('express-session')({
    secret: 'keyboard cat',
    resave: false,
    saveUninitialized: false
}));
app.use(passport.initialize());
app.use(passport.session());
app.use(function requireHTTPS(req, res, next) {
    if (!req.secure && req.url.indexOf('/api') <= -1) {
        return res.redirect('https://' + req.headers.host.replace(':8080', ':8443') + req.url);
    }
    next();
});
app.use(express.static(path.join(__dirname, "public")));
app.use('/', require('./app/routes/'));

// passport config
var Account = require('./app/models/account');
passport.use(new LocalStrategy(Account.authenticate()));
passport.serializeUser(Account.serializeUser());
passport.deserializeUser(Account.deserializeUser());

// Database
mongoose.connect(database.url);
var db = mongoose.connection;

db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function () {
    console.log('opened');
});

app.listen(8080, function () {
    console.log('Example app listening on port 3000!');
});


var privateKey = fs.readFileSync('./config/domain.key');
var certificate = fs.readFileSync('./config/domain.crt');

https.createServer({
    key: privateKey,
    cert: certificate
}, app).listen(8443);