'use strict'

var app = require('express'),
    router = app.Router();

var CurrentSample = require('../models/currentSample');
var VoltageSample = require('../models/voltageSample');
var ReactiveSample = require('../models/reactiveSample');
var ActiveSample = require('../models/activeSample');
var Blackout = require('../models/blackout');

var auth = function (req, res, next) {
    if (!req.isAuthenticated()) {
        res.send(401);
    } else {
        next();
    }
};

//API: Get all entries
router.get('/data/:timestamp', function (req, res) {
    var timestamp = new Date(0);
    timestamp.setUTCSeconds(req.params.timestamp);
    var error = false;
    var callbacks = 0;
    var data = {
        "status": "",
        "voltage": [],
        "current": [],
        "reactive": [],
        "active": []
    };

    VoltageSample.find({
        time: {
            $gt: timestamp.toISOString()
        }
    }).lean().exec(function (err, samples) {
        if (err) {
            error = true;
            return console.error(err);
        }
        data.voltage = samples;
    });

    CurrentSample.find({
        time: {
            $gt: timestamp.toISOString()
        }
    }).lean().exec(function (err, samples) {
        if (err) {
            error = true;
            return console.error(error);
        }
        data.current = samples;
    });

    ActiveSample.find({
        time: {
            $gt: timestamp.toISOString()
        }
    }).lean().exec(function (err, samples) {
        if (err)
            return console.error(err);
        data.active = samples;
    });

    ReactiveSample.find({
        time: {
            $gt: timestamp.toISOString()
        }
    }).lean().exec(function (err, samples) {
        if (err)
            return console.error(err);
        data.reactive = samples;
        data.status = "OK";
        res.json(data);
    });

});


/* VRMS */
router.get('/:deviceID/vrms/:from', auth, function (req, res) {
    console.log("from vrms");
    VoltageSample.find({
        device: {
            $eq: req.params.deviceID
        },
        time: {
            $gt: req.params.from
        }
    }).lean().exec(function (err, samples) {
        if (err)
            return console.error(err);
        //        console.log(samples);
        res.json(samples);

    });
});

router.get('/:deviceID/vrms/:from/:to', auth, function (req, res) {
    console.log("from to vrms");

    VoltageSample.find({
        device: {
            $eq: req.params.deviceID
        },
        time: {
            $gt: req.params.from,
            $lt: req.params.to
        }
    }).lean().exec(function (err, samples) {
        if (err)
            return console.error(err);
        //        console.log(samples);
        res.json(samples);

    });
});


/* IRMS */
router.get('/:deviceID/irms/:from', auth, function (req, res) {
    console.log("from irms");

    CurrentSample.find({
        device: {
            $eq: req.params.deviceID
        },
        time: {
            $gt: req.params.from
        }
    }).lean().exec(function (err, samples) {
        if (err)
            return console.error(err);
        //        console.log(samples);
        res.json(samples);
    });


});

router.get('/:deviceID/irms/:from/:to', auth, function (req, res) {
    console.log("from to irms");

    CurrentSample.find({
        device: {
            $eq: req.params.deviceID
        },
        time: {
            $gt: req.params.from,
            $lt: req.params.to
        }
    }).lean().exec(function (err, samples) {
        if (err)
            return console.error(err);
        //        console.log(samples);
        res.json(samples);
    });


});


/* PWRR */
router.get('/:deviceID/reactivepwr/:from', auth, function (req, res) {
    console.log("from reac");

    ReactiveSample.find({
        device: {
            $eq: req.params.deviceID
        },
        time: {
            $gt: req.params.from
        }
    }).lean().exec(function (err, samples) {
        if (err)
            return console.error(err);
        res.json(samples);
    });
});

router.get('/:deviceID/reactivepwr/:from/:to', auth, function (req, res) {
    console.log("from to reac");

    ReactiveSample.find({
        device: {
            $eq: req.params.deviceID
        },
        time: {
            $gt: req.params.from,
            $lt: req.params.to
        }
    }).lean().exec(function (err, samples) {
        if (err)
            return console.error(err);
        res.json(samples);
    });
});


/* PWRA */
router.get('/:deviceID/activepwr/:from', auth, function (req, res) {
    console.log("from act");

    ActiveSample.find({
        device: {
            $eq: req.params.deviceID
        },
        time: {
            $gt: req.params.from
        }
    }).lean().exec(function (err, samples) {
        if (err)
            return console.error(err);
        res.json(samples);
    });
});

router.get('/:deviceID/activepwr/:from/:to', auth, function (req, res) {
    console.log("from to act");

    ActiveSample.find({
        device: {
            $eq: req.params.deviceID
        },
        time: {
            $gt: req.params.from,
            $lt: req.params.to
        }
    }).lean().exec(function (err, samples) {
        if (err)
            return console.error(err);
        res.json(samples);
    });
});

//API: Create a new entry
router.post('/', function (req, res) {
    var current = new CurrentSample(req.body.current);
    var voltage = new VoltageSample(req.body.voltage);
    var active = new ActiveSample(req.body.active);
    var reactive = new ReactiveSample(req.body.reactive);

    var timestamp = req.body.time;
    var device = req.body.device;

    current.time = req.body.time;
    voltage.time = req.body.time;
    active.time = req.body.time;
    reactive.time = req.body.time;

    current.device = req.body.device;
    voltage.device = req.body.device;
    active.device = req.body.device;
    reactive.device = req.body.device;

    var errorVoltage = "";
    var errorCurrent = "";
    var errorReactive = "";
    var errorActive = "";
    var error = false;

    voltage.save(function (err) {
        if (err) {
            errorVoltage = err;
            error = true;
        }
    });

    current.save(function (err) {
        if (err) {
            errorCurent = err;
            error = true;
        }
    });

    active.save(function (err) {
        if (err) {
            errorActive = err;
            error = true;
        }
    });

    reactive.save(function (err) {
        if (err) {
            errorActive = err;
            error = true;
        }
    });

    var resContent;
    if (!error) {
        resContent = {
            status: 'OK'
        };
    } else {
        resContent = {
            status: 'FAIL',
            message: {
                voltage: errorVoltage,
                current: errorCurrent,
                active: errorActive,
                reactive: errorReactive
            }
        };
    }

    res.send(resContent);
});

router.post("/blackout", function (req, res) {
    var blackout = new Blackout(req.body);
    blackout.save(function (err) {
        if (err) {
            res.json({
                status: 'Fail',
                message: err
            })
        } else {
            res.json({
                status: 'OK',
                message: 'Data succesfully added.'
            })
        }
    });
});

module.exports = router;