var router = require('express').Router();
var passport = require('passport');
var pdfkit = require('pdfkit');
var Account = require('../models/account');
var Device = require("../models/device");
var Blackout = require("../models/blackout");


// middleware functions
var auth = function (req, res, next) {
    if (!req.isAuthenticated()) {
        res.sendStatus(401);
        //res.send("NO");
    } else {
        next();
        //res.send("SI");
    }
};

var myself = function (req, res, next) {
    if (req.params.userID != req.user._id)
        res.sendStatus(401);
    else
        next()
};


// Register
router.post('/register', function (req, res) {
    var acc = new Account({
        username: req.body.username,
        regTime: new Date().toISOString()
    });
    console.log(acc);
    Account.register(acc, req.body.password, function (err, account) {
        if (err) {
            return res.send('reg fail' + err);
            console.log(err);
        }

        passport.authenticate('local')(req, res, function () {
            res.redirect('/misdatos');
        });
    });
});


// Login
router.get('/login', auth, function (req, res) {
    res.send(req.user);
});

router.post('/login', passport.authenticate('local', {
    successRedirect: '/misdatos',
    failureRedirect: '/login'
}));

router.get('/logout', function (req, res) {
    req.logout();
    res.redirect('/');
});


// Test route
router.get('/ping', function (req, res) {
    res.status(200).send("pong!");
});


// User info
router.get('/:userID', auth, myself, function (req, res) {
    Account.findById(req.params.userID)
        .lean().exec(function (err, account) {
            if (err)
                return console.error(err);
            res.json(account);
        });
});

router.post('/:userID', auth, myself, function (req, res) {
    Account.findOne({
        _id: req.user._id
    }, function (err, doc) {
        if (err) {
            return console.log(err);
        }

        if (req.query.devID) {
            Device.findOne({
                    "ID": {
                        $eq: req.query.devID
                    }
                },
                function (err, device) {

                    if (err) {
                        return console.log(err);
                    }

                    if (device) {
                        var none = true;
                        for (var i in doc.devices) {
                            if (doc.devices[i].ID == req.query.devID) {
                                none = false;
                                break;
                            }
                        }
                        if (none) {
                            doc.devices.push({
                                ID: req.query.devID,
                                name: req.query.devName,
                                dev_id: device._id
                            });
                        }
                    }
                    doc.save(function () {
                        console.log(doc);
                        res.json(doc);
                    });
                });

        } else {
            doc.realName = (req.query.first_name) ? req.query.first_name : doc.realName;
            doc.realSurname = (req.query.last_name) ? req.query.last_name : doc.realSurname;
            doc.phone = (req.query.phone) ? req.query.phone : doc.phone;
            doc.location = (req.query.location) ? req.query.location : doc.location;
            doc.mail = (req.query.mail) ? req.query.mail : doc.mail;
            doc.save(function () {
                res.json(doc);
            });
        }
    });


});


// Blackout routes
router.get("/:userID/blackout/", auth, myself, function (req, res) {

    Account.findOne({
        _id: {
            $eq: req.params.userID
        }
    }).lean().exec(function (err, user) {
        if (err)
            return console.error(err);

        var deviceIDs = (user.devices).map(function (device) {
            return device.ID;
        });

        Blackout.find({
            ID: {
                $in: deviceIDs
            }
        }).lean().exec(function (err, blackouts) {
            if (err)
                return console.error(err);
            res.json(blackouts);
        });
    });

});

router.get("/:userID/blackout/:blackoutID", auth, function (req, res) {
    Blackout.find({
        _id: {
            $eq: req.params.blackoutID
        }
    }).lean().exec(function (err, blackouts) {
        if (err)
            return console.error(err);
        res.json(blackouts);
    });

});

// Device routes
router.get("/:userID/device/", auth, myself, function (req, res) {

    Account.findOne({
        _id: {
            $eq: req.params.userID
        }
    }).lean().exec(function (err, user) {
        if (err)
            return console.error(err);
        res.json(user.devices);
    });

});

router.post("/:userID/device/:deviceID", auth, function (req, res) {
    Device.findOne({
            "ID": {
                $eq: req.query.devID
            }
        },
        function (err, device) {

            if (err) {
                return console.log(err);
            }

            if (device) {
                var none = true;
                for (var i in doc.devices) {
                    if (doc.devices[i].ID == req.query.devID) {
                        none = false;
                        break;
                    }
                }
                if (none) {
                    doc.devices.push({
                        ID: req.query.devID,
                        name: req.query.devName
                    });
                }
            }
            doc.save(function () {
                console.log("a");
                res.json(doc);
            });
        });

    Blackout.find({
        _id: {
            $eq: req.params.blackoutID
        }
    }).lean().exec(function (err, blackouts) {
        if (err)
            return console.error(err);
        res.json(blackouts);
    });

});


// Certificates
router.get('/:userID/certificate/:blackoutID', auth, function (req, res) {
    var doc = new pdfkit();
    var callback = function (blackout, device) {
        console.log(blackout);
        console.log(device);
        doc.fontSize(24);
        doc.text('Certificado de caída de suministro', {
            underline: true,
            align: 'center'
        }).moveDown(2);

        doc.fontSize(12);
        doc.text('La empresa NetAlt, con NIF: 12345689, con sede en Campus Lagoas-Marcosende, Rúa Maxwell, s/n, 36310 Vigo, Pontevedra, Pontevedra.').moveDown(4);

        doc.fontSize(18);
        doc.text("Certifica:", {
            underline: true,
            align: 'center'
        }).moveDown(1);

        doc.fontSize(12);
        doc.text('Que se ha producido un corte de suministro eléctrico registrado en el dispositivo net alt con identificador ' + (device._id.toString()).toUpperCase() + ' entre las siguientes fechas:').moveDown();

        doc.text('Fecha de inicio:');
        doc.text('Fecha de finalización:').moveUp(2);
        doc.text((new Date(blackout.startTime)).toLocaleString(), 200);
        doc.text((new Date(blackout.endTime)).toLocaleString(), 200);
        doc.end();

        doc.pipe(res);
    }


    Blackout.findOne({
        _id: {
            $eq: req.params.blackoutID
        }
    }).lean().exec(function (err, blkout) {
        if (err) {
            res.send(404);
            return console.error(err);
        }
        //blackout = blkout;
        Device.findOne({
            ID: {
                $eq: blkout.ID
            }
        }).lean().exec(function (err, dev) {
            if (err) {
                res.send(404);
                return console.error(err);
            }
            callback(blkout, dev);
        })
    });


});


//router.get('/estadisticas', auth, function (req, res) {
//    res.sendFile('estadisticas.html', {
//        root: __dirname + '/../../public/estadisticas/'
//    });
//})

module.exports = router;