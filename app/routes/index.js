'use strict'

var router = require('express').Router();
router.use('/api', require('./api'));
router.use('/user', require('./user'));
router.use('*', require('./public'));

module.exports = router;