'use strict'

var app = require('express'),
    router = app.Router();

var auth = function (req, res, next) {
    if (!req.isAuthenticated()) {
        res.send('not logged');
    } else {
        //        next();
        res.send('logged');
    }
};


router.get('*', function (req, res) {
    res.sendFile('index.html', {
        root: __dirname + '/../../public/'
    });
});





module.exports = router;