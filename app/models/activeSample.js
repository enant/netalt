'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var activeSchema = new Schema({
    value: Number,
    time: Date,
    device: String
});

module.exports = mongoose.model('ActiveSample', activeSchema);