'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var reactivaSchema = new Schema({
    value: Number,
    time: Date,
    device: String
});

module.exports = mongoose.model('ReactiveSample', reactivaSchema);