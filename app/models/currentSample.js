'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var corrienteSchema = new Schema({
    value: Number,
    time: Date,
    device: String
});


module.exports = mongoose.model('CurrentSample', corrienteSchema);