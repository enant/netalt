var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Device = new Schema({
    ID: String,
    name: String
});


module.exports = mongoose.model('Device', Device);