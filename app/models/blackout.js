'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var blackoutSchema = new Schema({
    ID: String,
    startTime: Date,
    endTime: Date
});

module.exports = mongoose.model('Blackout', blackoutSchema);