var mongoose = require('mongoose');
var Device = require("./device");
var Schema = mongoose.Schema;
var passportLocalMongoose = require('passport-local-mongoose');

var Account = new Schema({
    username: String,
    password: String,
    device: String,
    realName: String,
    realSurname: String,
    phone: String,
    location: String,
    regTime: Date,
    mail: String,
    devices: [Device.schema]
});

Account.plugin(passportLocalMongoose);

module.exports = mongoose.model('Account', Account);