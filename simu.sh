#!/bin/bash
while true
do
	voltage=$[$[RANDOM%100]+1]
	current=$[$[RANDOM%100]+1]
	reactive=$[$[RANDOM%100]+1]
	active=$[$[RANDOM%100]+1]
	atime=$(date -Is)
	sleep 1s
	curl -H "Content-Type: application/json" -X POST -d '{"device":"simulacion","time":"'"$atime"'","voltage":{"value":"'"$voltage"'"},"current":{"value":"'"$current"'"},"active":{"value":"'"$active"'"},"reactive":{"value":"'"$reactive"'"}}' http://localhost:8080/api
done

