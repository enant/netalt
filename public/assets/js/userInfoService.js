'use strict';

angular.module('myApp.UserInfoService', ['ngRoute', 'ngResource'])

.factory('UserInfoService', function ($resource) {
    return $resource('/user/getUserInfo', {}, {
        'getUserInfo': {
            method: 'GET',
            isArray: false
        }
    }); // Note the full endpoint address
})

.controller(function () {

});