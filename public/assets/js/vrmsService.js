'use strict';

angular.module('myApp.VrmsService', ['ngRoute', 'ngResource'])

.factory('VrmsService', function ($resource) {
    return $resource('/api/:deviceID/vrms/:from/:to', {}, {
        'getNewVrms': {
            method: 'GET',
            isArray: true,
            to: ''
        }
    }); // Note the full endpoint address
})

.controller(function () {

});