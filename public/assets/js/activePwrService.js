'use strict';

angular.module('myApp.ActivePwrService', ['ngRoute', 'ngResource'])

.factory('ActivePwrService', function ($resource) {
    return $resource('/api/:deviceID/activepwr/:from/:to', {}, {
        'getNewActivePwr': {
            method: 'GET',
            isArray: true,
            to: '',
            deviceID: 'Prueba'
        }
    }); // Note the full endpoint address
})

.controller(function () {

});