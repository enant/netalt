'use strict';

angular.module('myApp.ReactivePwrService', ['ngRoute', 'ngResource'])

.factory('ReactivePwrService', function ($resource) {
    return $resource('/api/:deviceID/reactivepwr/:from/:to', {}, {
        'getNewReactivePwr': {
            method: 'GET',
            isArray: true,
            to: '',
            deviceID: 'Prueba'
        }
    }); // Note the full endpoint address
})

.controller(function () {

});