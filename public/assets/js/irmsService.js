'use strict';

angular.module('myApp.IrmsService', ['ngRoute', 'ngResource'])

.factory('IrmsService', function ($resource) {
    return $resource('/api/:deviceID/irms/:from/:to', {}, {
        'getNewIrms': {
            method: 'GET',
            isArray: true,
            to: '',
            deviceID: 'Prueba'
        }
    }); // Note the full endpoint address
})

.controller(function () {

});