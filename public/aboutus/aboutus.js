'use strict';

angular.module('myApp.aboutUs', ['ngRoute'])

.config(['$routeProvider', function ($routeProvider) {
    console.log('about');
    $routeProvider.when('/conocenos', {
        templateUrl: 'aboutus/aboutus.html',
        controller: 'AboutUsCtrl'
    });


}])

.controller('AboutUsCtrl', ['$scope', '$interval', function ($scope, $interval) {

}]);