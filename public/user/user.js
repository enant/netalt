'use strict';

var checkLoggedin = function ($q, $timeout, $http, $location, $rootScope) {
    // Initialize a new promise
    var deferred = $q.defer();
    $http.get('/user/login').then(function (response) {
        deferred.resolve();
    }, function (response) {
        $rootScope.message = 'You need to log in.';
        deferred.reject();
        $location.url('/login');
    });

    return deferred.promise;
};
angular.module('myApp.user', ['ngRoute'])

.config(['$routeProvider', function ($routeProvider) {
    $routeProvider.when('/misdatos', {
        templateUrl: 'user/user.html',
        controller: 'UserCtrl',
        resolve: {
            loggedin: checkLoggedin
        }
    });


}])

.controller('UserCtrl', ['$scope', "UserInfoService", '$resource', function ($scope, UserInfoService, $resource) {

    var Login = $resource('user/login');
    var User = $resource('/user/:userID/');
    var Blackout = $resource('/user/:userID/blackout/:blackoutID');

    $scope.account = {};
    $scope.blackouts = [];

    var loginInfo = Login.get(function () {
        $scope.account = User.get({
            userID: loginInfo._id
        }, function () {
            $scope.account.regTime = (new Date($scope.account.regTime)).toLocaleDateString();
            $scope.blackouts = Blackout.query({
                userID: loginInfo._id
            });
        });
    });

    $scope.submitDevice = function () {
        $scope.account.$save({
            userID: $scope.account._id,
            devID: this.devID,
            devName: this.devName
        }, function () {
            $scope.account.regTime = (new Date($scope.account.regTime)).toLocaleDateString();
            $scope.blackouts = Blackout.query({
                userID: loginInfo._id
            });
        });
    };

    $scope.submitData = function () {
        $scope.account.$save({
            userID: $scope.account._id,
            first_name: this.first_name,
            last_name: this.last_name,
            phone: this.phone,
            location: this.location,
            mail: this.email
        }, function () {
            $scope.account.regTime = (new Date($scope.account.regTime)).toLocaleDateString();
        });
    };



}]);