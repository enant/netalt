'use strict';

Highcharts.setOptions({
    global: {
        useUTC: false
    }
});

/* Function that checks if user is logged in */
var checkLoggedin = function ($q, $timeout, $http, $location, $rootScope) {
    // Initialize a new promise
    var deferred = $q.defer();
    $http.get('/user/login').then(function (response) {
        deferred.resolve();
    }, function (response) {
        $rootScope.message = 'You need to log in.';
        deferred.reject();
        $location.url('/login');
    });

    return deferred.promise;
};

/* Module */
angular.module('myApp.estadisticas', ['ngRoute'])

.config(['$routeProvider', function ($routeProvider) {
    $routeProvider.when('/estadisticas', {
        templateUrl: 'estadisticas/estadisticas.html',
        controller: 'EstadisticasCtrl',
        resolve: {
            loggedin: checkLoggedin
        }
    });
}])

.controller('EstadisticasCtrl', ['$scope', '$interval', 'VrmsService', 'IrmsService', 'ReactivePwrService', 'ActivePwrService', '$resource', function ($scope, $interval, VrmsService, IrmsService, ReactivePwrService, ActivePwrService, $resource) {
    /* Services */
    var Login = $resource('/user/login');
    var UserDevices = $resource('/user/:userID/device');

    /* Charts config */
    $scope.powerChart = {
        options: {
            chart: {
                type: 'line',
                zoomType: 'x'
            },
            xAxis: {
                type: 'datetime'
            },
            yAxis: [{ // Voltage yAxis
                labels: {
                    format: '{value} W'
                },
                title: {
                    text: 'Active power'
                },
                opposite: true

    }, { // Current yAxis
                gridLineWidth: 0,
                title: {
                    text: 'Reactive power'
                },
                labels: {
                    format: '{value} W'
                }

    }],
            plotOptions: {
                line: {
                    marker: {
                        enabled: false
                    }
                }
            }
        },
        series: [{
            name: 'Potencia reactiva',
            data: []
    }, {
            name: 'Potencia activa',
            yAxis: 1,
            data: []
    }],
        title: {
            text: 'Reactive and active power'
        },
        loading: false
    };
    $scope.currentVoltageChart = {
        options: {
            chart: {
                type: 'line',
                zoomType: 'x'
            },
            xAxis: {
                type: 'datetime'
            },
            yAxis: [{ // Voltage yAxis
                labels: {
                    format: '{value} V'
                },
                title: {
                    text: 'Voltage'
                },
                opposite: true

    }, { // Current yAxis
                gridLineWidth: 0,
                title: {
                    text: 'Current'
                },
                labels: {
                    format: '{value} mA'
                }

    }],
            plotOptions: {
                line: {
                    marker: {
                        enabled: false
                    }
                }
            }
        },
        series: [{
            name: 'V_rms',
            data: []
    }, {
            name: 'I_rms',
            yAxis: 1,
            data: []
    }],
        title: {
            text: 'RMS Current and Voltage'
        },
        loading: false
    };

    /* Chart series */
    $scope.currentVoltageSeries = $scope.currentVoltageChart.series;
    $scope.powerSeries = $scope.powerChart.series;

    /* Model for devices select */
    $scope.activeDevice = {};

    /* Start time for plot */
    $scope.date = new Date();
    $scope.date.setMinutes($scope.date.getMinutes() - 3);
    $scope.date = $scope.date.toISOString();

    /* Look for data every second */
    var promise = $interval(ticker, 1000);

    /* Load user devices */
    $scope.devices = [];
    $scope.login = Login.get(function () {
        $scope.devices = UserDevices.query({
            userID: $scope.login._id
        }, function () {
            $scope.activeDevice = $scope.devices[0]
        });
    });

    /* Populates series */
    var dataToSeries = function (newdata, series) {
        //var s = $scope.currentVoltageSeries;
        for (var i = 0; i < newdata.length; i++) {
            series.data = series.data.concat([[new Date(newdata[i].time).getTime(), newdata[i].value]]);
        }
        if (newdata.length != 0)
            $scope.date = newdata[newdata.length - 1].time;
    };

    /* Clears series */
    $scope.clearPlot = function () {
        $scope.currentVoltageSeries[0].data = [];
        $scope.currentVoltageSeries[1].data = [];
        $scope.powerSeries[0].data = [];
        $scope.powerSeries[1].data = [];
        $scope.date = new Date();
        $scope.date.setMinutes($scope.date.getMinutes() - 3);
        $scope.date = $scope.date.toISOString();
    };

    /* Show range */
    $scope.submitNewRange = function () {
        if (promise) $interval.cancel(promise);
        console.log("SUBMIT");
        var dateRegEx = /(\d{2})-(\d{2})-(\d{4}) (\d{2}):(\d{2}):(\d{2})/g;
        console.log(this.startDateTime + " " + this.endDateTime);

        var psd = dateRegEx.exec(this.startDateTime);
        dateRegEx.lastIndex = 0;
        var ped = dateRegEx.exec(this.endDateTime);
        var startDate;
        var endDate;

        console.log(psd[3] + "-" + psd[2] + "-" + psd[1] + "T" + psd[4] + ":" + psd[5] + ":" + psd[6] + (new Date()).getTimezoneOffset() / 60 + " *** " + ped[3] + "-" + ped[2] + "-" + ped[1] + "T" + ped[4] + ":" + ped[5] + ":" + ped[6]);

        if (!ped || !psd) {
            $scope.startDateStatus = (!psd) ? "has-error" : "has-success";
            $scope.endDateStatus = (!ped) ? "has-error" : "has-success";

        }
        try {
            startDate = new Date();
            startDate.setFullYear(psd[3]);
            startDate.setMonth(psd[2] - 1);
            startDate.setDate(psd[1]);
            startDate.setHours(psd[4]);
            startDate.setMinutes(psd[5]);
            startDate.setSeconds(psd[6]);
            startDate.setMilliseconds(0);
        } catch (err) {}

        try {
            endDate = new Date();
            endDate.setFullYear(ped[3]);
            endDate.setMonth(ped[2] - 1);
            endDate.setDate(ped[1]);
            endDate.setHours(ped[4]);
            endDate.setMinutes(ped[5]);
            endDate.setSeconds(ped[6]);
            endDate.setMilliseconds(0);

        } catch (err2) {}

        if (!startDate || !endDate) {
            $scope.startDateStatus = (!startDate) ? "has-error" : "has-success";
            $scope.endDateStatus = (!endDate) ? "has-error" : "has-success";
            return;
        }

        $interval.cancel(promise);

        var vrms = VrmsService.getNewVrms({
            from: startDate,
            to: endDate,
            deviceID: $scope.activeDevice.ID
        }, function () {
            var s = $scope.currentVoltageSeries;
            s[0].data = [];
            dataToSeries(vrms, s[0]);
        });

        var irms = IrmsService.getNewIrms({
            from: startDate,
            to: endDate,
            deviceID: $scope.activeDevice.ID
        }, function () {
            var s = $scope.currentVoltageSeries;
            s[1].data = [];
            dataToSeries(irms, s[1]);
        });

        var activepwr = ActivePwrService.getNewActivePwr({
            from: startDate,
            to: endDate,
            deviceID: $scope.activeDevice.ID
        }, function () {
            var s = $scope.powerSeries;
            s[1].data = [];
            dataToSeries(activepwr, s[1]);
        });

        var reactivepwr = ReactivePwrService.getNewReactivePwr({
            from: startDate,
            to: endDate,
            deviceID: $scope.activeDevice.ID
        }, function () {
            var s = $scope.powerSeries;
            s[0].data = [];
            dataToSeries(reactivepwr, s[0]);
        });
    }

    /* Start live data */
    $scope.startLive = function () {
        $scope.clearPlot();
        $scope.date = new Date();
        $scope.date.setMinutes($scope.date.getMinutes() - 3);
        $scope.date = $scope.date.toISOString();
        if (promise) $interval.cancel(promise);
        promise = $interval(ticker, 1000);
    }

    /* Stop ticker when location is changed */
    $scope.$on('$destroy', function (event) {
        $interval.cancel(promise);
    });

    /* Ticker for live data */
    function ticker() {
        console.log($scope.activeDevice);
        var vrms = VrmsService.getNewVrms({
            deviceID: $scope.activeDevice.ID,
            from: $scope.date
        }, function () {
            var s = $scope.currentVoltageSeries;
            dataToSeries(vrms, s[0]);
        });

        var irms = IrmsService.getNewIrms({
            from: $scope.date,
            deviceID: $scope.activeDevice.ID
        }, function () {
            var s = $scope.currentVoltageSeries;
            dataToSeries(irms, s[1]);
        });

        var activepwr = ActivePwrService.getNewActivePwr({
            from: $scope.date,
            deviceID: $scope.activeDevice.ID
        }, function () {
            var s = $scope.powerSeries;
            dataToSeries(activepwr, s[1]);
        });

        var reactivepwr = ReactivePwrService.getNewReactivePwr({
            from: $scope.date,
            deviceID: $scope.activeDevice.ID
        }, function () {
            var s = $scope.powerSeries;
            dataToSeries(reactivepwr, s[0]);
        });
    };

}]);