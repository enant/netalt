'use strict';

// Declare app level module which depends on views, and components
angular.module('myApp', [
    'ngRoute',
    'ngResource',
    'myApp.inicio',
    'myApp.estadisticas',
    'myApp.user',
    'myApp.VrmsService',
    'myApp.IrmsService',
    'myApp.ActivePwrService',
    'myApp.ReactivePwrService',
    'myApp.UserInfoService',
    'myApp.Register',
    'myApp.login',
    'myApp.aboutUs',
    'myApp.savings',
    'highcharts-ng'
]).

config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
    $routeProvider.otherwise({
        redirectTo: '/inicio'
    });
    $locationProvider.html5Mode(true);
}]).

controller('NavController', ['$scope', '$location', function ($scope, $location) {
    $scope.isActive = function (viewLocation) {
        return viewLocation === $location.path();
    };
}]);