'use strict';

angular.module('myApp.inicio', ['ngRoute'])

.config(['$routeProvider', function ($routeProvider) {
    console.log('inicio');
    $routeProvider.when('/inicio', {
        templateUrl: 'inicio/inicio.html',
        controller: 'InicioCtrl'
    });


}])

.controller('InicioCtrl', ['$scope', '$interval', function ($scope, $interval) {

}]);