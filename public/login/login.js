'use strict';

angular.module('myApp.login', ['ngRoute'])

.config(['$routeProvider', function ($routeProvider) {
    console.log('login');
    $routeProvider.when('/login', {
        templateUrl: 'login/login.html',
        controller: 'LoginCtrl'
    });


}])

.controller('LoginCtrl', ['$scope', '$interval', function ($scope, $interval) {

}]);